# Instructions

Please view the Jupyter notebook ([`speech-to-text.ipynb`][speech-to-text.ipynb viewer]) that is provided in this repository.


# About

This project transcribe speech to text with reasonably high accuracy using a small (~40 MB), pre-trained model.
Transcriptions can be processed 6 times the speed of realtime on an average computer in 2022.


# Purpose

Speech to text has many practical applications. This includes:

- converting audio lectures and recorded journal entries to text for faster searching
- on-the-fly video meetings to subtitles for when words could not be deciphered by ear
- condensing audio files into text for long-term storage


# Demo

![Demo transcription video][Demo transcription video]

Correct transcription:

> When did you come to Australia? The Kelpie, an Australian sheep dog, is an excellent working dog and can muster large numbers of sheep with little or no command guidance. I can't work out what you're on about. You have a sense of humour. I like that.


[speech-to-text.ipynb viewer]: notebooks/speech-to-text.ipynb
[Demo transcription video]: notebooks/downloads/video/demo.mp4
