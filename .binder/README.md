# About

The contents of this directory is for configuring [MyBinder](https://mybinder.org/),
a free service for running interactive Jupyter notebooks.
For more information, view the [documentation for MyBinder configuration][MyBinder config docs]
(`repo2docker` is used internally).


## Using MyBinder on this repository

A live demo link should be available in either this project's description
or in `README.md` located at the project's root directory.


[MyBinder config docs]: https://mybinder.readthedocs.io/en/latest/using/config_files.html
