FROM gitpod/workspace-python-3.9:2022-11-15-17-00-18

USER gitpod

RUN sudo apt-get update \
 && sudo apt-get install -y \
    curl \
    jo \
    tar \
    unzip \
 && sudo rm -rf /var/lib/apt/lists/*
