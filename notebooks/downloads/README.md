# Info

This directory contains resources that are used in the demo Jupyter notebook.
Files will be downloaded from the source as needed in the Jupyter notebook,
so it is safe to remove this directory.
It has been included for posterity in case they no longer exists at the source.

## License

Files have been reproduced with permission from the source.

- `audio/`: Creative Commons (CC). See Jupyter notebook for flavour (BY-NC, BY-SA, etc) and attributions
- `data/`: CC BY 2.0 FR. Downloaded from https://tatoeba.org/en/downloads
- `model/`: Apache 2.0. Downloaded from https://alphacephei.com/vosk/models#model-list
